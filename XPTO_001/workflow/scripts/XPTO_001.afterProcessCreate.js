/**
 *
 * @desc        Script que será executado logo após a criação da solicitação no Fluig
 * @copyright   2021 SkFly
 * @version     1.0.0
 * @author      Kelvin Hudson <kelvin@skfly.com.br>
 *
 */

function afterProcessCreate(processId) {

	// salva o número da solicitação no campo do formulário
	hAPI.setCardValue("PROTOCOLO", processId);
    
    // salva a data no campo do formulário
    var dtaHra = dataFormatada();
	hAPI.setCardValue('DTABERTURA', dtaHra);
	
}

/**
 * @desc 	Retorna a Data e Hora atual no formato: DD/MM/AAAA hh:mm:ss 
 */
function dataFormatada() {
    var data = new Date(),
        dia = data.getDate(),
        mes = data.getMonth() + 1,
        ano = data.getFullYear(),
        hora = data.getHours(),
        minutos = data.getMinutes(),
        segundos = data.getSeconds();
    
    var dtaHra = '';
    dtaHra += [Number(dia).pad(), Number(mes).pad(), Number(ano).pad()].join('/');
    //dtaHra += ' ';
    //dtaHra += [Number(hora).pad(), Number(minutos).pad(), Number(segundos).pad()].join(':');
    
    return dtaHra;
}

/**
 * @desc 	Formata um número colocando @size zeros à esquerda  
 */
Number.prototype.pad = function (size) {
    var s = String(this);
    while (s.length < (size || 2)) {
        s = "0" + s;
    }
    return s;
}