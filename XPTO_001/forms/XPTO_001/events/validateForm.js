function validateForm(form){
	
	var mensagemErro = "";

	var atividadeAtual = parseInt(getValue("WKNumState"));

	if(  atividadeAtual == 0 || atividadeAtual == ATV.INICIO){
		
		mensagemErro += form.getValue("CEP_ORIGEM") != "" ? "" : "O campo \"CEP origem\" é de preenchimento obrigatório. <br>";
		mensagemErro += form.getValue("LOGRADOURO_ORIGEM") != "" ? "" : "O campo \"Logradouro origem\" é de preenchimento obrigatório. <br>";
		mensagemErro += form.getValue("NUMERO_ORIGEM") != "" ? "" : "O campo \"Número origem\" é de preenchimento obrigatório. <br>";
		mensagemErro += form.getValue("COMPLEMENTO_ORIGEM") != "" ? "" : "O campo \"Complemento origem\" é de preenchimento obrigatório. <br>";
		mensagemErro += form.getValue("BAIRRO_ORIGEM") != "" ? "" : "O campo \"Bairro origem\" é de preenchimento obrigatório. <br>";
		mensagemErro += form.getValue("UF_ORIGEM") != "" ? "" : "O campo \"UF origem\" é de preenchimento obrigatório. <br>";
		mensagemErro += form.getValue("CIDADE_ORIGEM") != "" ? "" : "O campo \"Cidade origem\" é de preenchimento obrigatório. <br>";
	
		mensagemErro += form.getValue("CODCOLIGADA") != "" ? "" : "O campo \"Empresa destino\" é de preenchimento obrigatório. <br>";
		mensagemErro += form.getValue("FILIAL_DESTINO") != "" ? "" : "O campo \"Filial destino\" é de preenchimento obrigatório. <br>";
		mensagemErro += form.getValue("CEP_DESTINO") != "" ? "" : "O campo \"CEP destino\" é de preenchimento obrigatório. <br>";
		mensagemErro += form.getValue("LOGRADOURO_DESTINO") != "" ? "" : "O campo \"Logradouro destino\" é de preenchimento obrigatório. <br>";
		mensagemErro += form.getValue("NUMERO_DESTINO") != "" ? "" : "O campo \"Número destino\" é de preenchimento obrigatório. <br>";
		mensagemErro += form.getValue("COMPLEMENTO_DESTINO") != "" ? "" : "O campo \"Complemento destino\" é de preenchimento obrigatório. <br>";
		mensagemErro += form.getValue("BAIRRO_DESTINO") != "" ? "" : "O campo \"Bairro destino\" é de preenchimento obrigatório. <br>";
		mensagemErro += form.getValue("UF_DESTINO") != "" ? "" : "O campo \"UF destino\" é de preenchimento obrigatório. <br>";
		mensagemErro += form.getValue("CIDADE_DESTINO") != "" ? "" : "O campo \"Cidade destino\" é de preenchimento obrigatório. <br>";
		
		mensagemErro += form.getValue("MERCADORIA") != "" ? "" : "O campo \"Remessa / Mercadoria\" é de preenchimento obrigatório. <br>";
		mensagemErro += form.getValue("DESCRICAO") != "" ? "" : "O campo \"Descrição\" é de preenchimento obrigatório. <br>";
	
	}

	if(  atividadeAtual == ATV.RECEBER_ITEM){
		
		mensagemErro += form.getValue("APROVACAO") != "" ? "" : "O campo \"Aprovado / Reprovado\" é de preenchimento obrigatório. <br>";
		mensagemErro += form.getValue("OBS_APROV") != "" ? "" : "O campo \"Observações\" é de preenchimento obrigatório. <br>";
	
	}


	
	if(mensagemErro != ""){
		
		//Adiciona espaço superior e inferior
		mensagemErro = "<br>" + mensagemErro + "<br>";
		
		throw  mensagemErro;
	}
	
}