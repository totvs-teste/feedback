function enableFields(form){

	var atividadeAtual = parseInt(getValue("WKNumState"));

    form.setEnabled("PROTOCOLO",false);
    form.setEnabled("NOME",false);
    form.setEnabled("DTABERTURA",false);

    form.setEnabled("LOGRADOURO_ORIGEM",false);
    form.setEnabled("BAIRRO_ORIGEM",false);
    form.setEnabled("UF_ORIGEM",false);
    form.setEnabled("CIDADE_ORIGEM",false);

    form.setEnabled("CEP_DESTINO",false);
    form.setEnabled("LOGRADOURO_DESTINO",false);
    form.setEnabled("NUMERO_DESTINO",false);
    form.setEnabled("COMPLEMENTO_DESTINO",false);
    form.setEnabled("BAIRRO_DESTINO",false);
    form.setEnabled("UF_DESTINO",false);
    form.setEnabled("CIDADE_DESTINO",false);

    if(atividadeAtual != ATV.INICIO && atividadeAtual != 0){
        form.setEnabled("CEP_ORIGEM",false); 
        form.setEnabled("NUMERO_ORIGEM",false); 
        form.setEnabled("COMPLEMENTO_ORIGEM",false);

        form.setEnabled("CODCOLIGADA",false); 
        form.setEnabled("FILIAL_DESTINO",false); 

        form.setEnabled("MERCADORIA",false); 
        form.setEnabled("DESCRICAO",false); 
    }


    if(atividadeAtual != ATV.RECEBER_ITEM){
        form.setEnabled("APROVACAO",false); 
        form.setEnabled("OBS_APROV",false); 
    }
    
    

    // if(form.getFormMode() != 'ADD'){
    //     form.setEnabled("CODFILIAL",false);
    // }


}