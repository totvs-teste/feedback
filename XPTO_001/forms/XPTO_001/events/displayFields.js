function displayFields(form,customHTML){

    var atividadeAtual = parseInt(getValue("WKNumState"));
    
    if(form.getFormMode() == 'ADD'){
        
        // resgata informações do usuário logado
        var filter = new java.util.HashMap();
        filter.put('colleaguePK.colleagueId', getValue('WKUser'));
        var solicitante = getDatasetValues('colleague', filter);
        form.setValue('NOME', solicitante.get(0).get("colleagueName"));
        form.setValue('PROTOCOLO', "-");
        form.setValue('DTABERTURA', "-");
        
    }
    
    if(atividadeAtual != ATV.INICIO && atividadeAtual != 0){
        if(form.getValue('APROVACAO') != "" || form.getFormMode() != "VIEW"){
            form.setVisibleById("divAprovacao",true);
        }
    }

    if(form.getValue("RESPONSAVEL_DESTINO") != ""){
        var filter = new java.util.HashMap();
        filter.put('colleaguePK.colleagueId', form.getValue("RESPONSAVEL_DESTINO"));
        var aprovador = getDatasetValues('colleague', filter);
        form.setValue('RESPONSAVEL_NOME', aprovador.get(0).get("colleagueName"));
        form.setValue('RESPONSAVEL_LOGIN', aprovador.get(0).get("login"));
    }

    customHTML.append("<script>");
    customHTML.append('$("form").addClass("'+form.getFormMode()+'")');
    customHTML.append("</script>");
    

}