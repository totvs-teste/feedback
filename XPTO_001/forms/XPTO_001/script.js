/**
 *
 * @desc        Script padrão do formulário
 * @copyright   2022 XPTO Inc
 * @version     1.0.0
 * @author      Kelvin Justino <kelvin.hudson@hotmail.com>
 *
 */

$(document).ready(function(){

	$("#CODCOLIGADA").change(function(){
		window["FILIAL_DESTINO"].clear();
		removedZoomItem({inputId:"FILIAL_DESTINO"});
		reloadZoomFilterValues('FILIAL_DESTINO', "CODCOLIGADA,"+$("#CODCOLIGADA").val());
	});

	$("#CEP_ORIGEM").change(buscaCep);
	attAprov();
	
	$('[data-toggle="buttons"] .btn-default [type="radio"]:checked').parent('label').addClass('active');
	$('[data-toggle="buttons"] .btn-default [type="radio"]:disabled').parent().attr("style","pointer-events: none;");


});

function buscaCep(){

	var CEP = $("#CEP_ORIGEM").val();
	CEP = CEP.replace("-","");

	var url = "https://viacep.com.br/ws/"+ CEP +"/json/?callback=?";

	$("[name='LOGRADOURO_ORIGEM'], [name='_LOGRADOURO_ORIGEM'] ").val("");
	$("[name='BAIRRO_ORIGEM'], [name='_BAIRRO_ORIGEM'] ").val("");
	$("[name='UF_ORIGEM'], [name='_UF_ORIGEM'] ").val("");
	$("[name='CIDADE_ORIGEM'], [name='_CIDADE_ORIGEM'] ").val("");

	$.getJSON(
		url,
		function(dados) {
			if (!("erro" in dados)) {
			
				$("[name='LOGRADOURO_ORIGEM'], [name='_LOGRADOURO_ORIGEM'] ").val(dados.logradouro);
				$("[name='BAIRRO_ORIGEM'], [name='_BAIRRO_ORIGEM'] ").val(dados.bairro);
				$("[name='UF_ORIGEM'], [name='_UF_ORIGEM'] ").val(dados.uf);
				$("[name='CIDADE_ORIGEM'], [name='_CIDADE_ORIGEM'] ").val(dados.localidade);
			} 
			else {
				FLUIGC.message.error({
					title: 'Falha ao buscar CEP!',
					message: 'Não foi possível encontrar endereço automaticamente, verifique o CEP informado.<br><br>',
					details: 'CEp não informado.'
				}, function(el, ev) {});
			}
		}
	);       


	
	
}

function setSelectedZoomItem(selectedItem) {
	var idCampo = selectedItem.inputId;

	switch (idCampo) {

		case "FILIAL_DESTINO":
			$("#RESPONSAVEL_DESTINO").val(selectedItem["RESPONSAVEL"]);
			$("#CEP_DESTINO, [name='_CEP_DESTINO']").val(selectedItem["CEP"]);
			$("#LOGRADOURO_DESTINO, [name='_LOGRADOURO_DESTINO']").val(selectedItem["LOGRADOURO"]);
			$("#NUMERO_DESTINO, [name='_NUMERO_DESTINO']").val(selectedItem["NUMERO"]);
			$("#COMPLEMENTO_DESTINO, [name='_COMPLEMENTO_DESTINO']").val(selectedItem["COMPLEMENTO"]);
			$("#BAIRRO_DESTINO, [name='_BAIRRO_DESTINO']").val(selectedItem["BAIRRO"]);
			$("#UF_DESTINO, [name='_UF_DESTINO']").val(selectedItem["UF"]);
			$("#CIDADE_DESTINO, [name='_CIDADE_DESTINO']").val(selectedItem["CIDADE"]);
			
			break;
	}

}

function removedZoomItem(removedItem) {
	var idCampo = removedItem.inputId;

	switch (idCampo) {

		case "FILIAL_DESTINO":
			$("#RESPONSAVEL_DESTINO").val("");
			$("#CEP_DESTINO, [name='_CEP_DESTINO']").val("");
			$("#LOGRADOURO_DESTINO, [name='_LOGRADOURO_DESTINO']").val("");
			$("#NUMERO_DESTINO, [name='_NUMERO_DESTINO']").val("");
			$("#COMPLEMENTO_DESTINO, [name='_COMPLEMENTO_DESTINO']").val("");
			$("#BAIRRO_DESTINO, [name='_BAIRRO_DESTINO']").val("");
			$("#UF_DESTINO, [name='_UF_DESTINO']").val("");
			$("#CIDADE_DESTINO, [name='_CIDADE_DESTINO']").val("");
			break;
	}	
}

function attAprov(){

	var nomeAprov = $("#RESPONSAVEL_NOME").val();
	nomeAprov = nomeAprov.toUpperCase();
	$("#spanNomeAprov").html(nomeAprov);
	
	var nomeLogin = $("#RESPONSAVEL_LOGIN").val();
	var imgLink = $("#imgAprov").attr("src");
	imgLink = imgLink.replace("admin",nomeLogin);
	$("#imgAprov").attr("src",imgLink);

}
