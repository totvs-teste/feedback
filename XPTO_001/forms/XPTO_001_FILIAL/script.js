/**
 *
 * @desc        Script padrão do formulário
 * @copyright   2022 XPTO Inc
 * @version     1.0.0
 * @author      Kelvin Justino <kelvin.hudson@hotmail.com>
 *
 */

$(document).ready(function(){

	$("#CEP").change(buscaCep);

	$("[name='CODAUTO']").change(function(){

		if($("[name='CODAUTO']:checked").val() == "Sim"){
			$("#divCodigoFilial").hide();
			$("#CODFILIAL").val("");
		}
		else{
			$("#divCodigoFilial").show();
		}

	});

});

function buscaCep(){

	var CEP = $("#CEP").val();
	CEP = CEP.replace("-","");

	var url = "https://viacep.com.br/ws/"+ CEP +"/json/?callback=?";


	$.getJSON(
		url,
		function(dados) {
			if (!("erro" in dados)) {
			
				$("[name='LOGRADOURO'], [name='_LOGRADOURO'] ").val(dados.logradouro);
				$("[name='BAIRRO'], [name='_BAIRRO'] ").val(dados.bairro);
				$("[name='UF'], [name='_UF'] ").val(dados.uf);
				$("[name='CIDADE'], [name='_CIDADE'] ").val(dados.localidade);
			} 
			else {
				FLUIGC.message.error({
					title: 'Falha ao buscar CEP!',
					message: 'Não foi possível encontrar endereço automaticamente, verifique o CEP informado.<br><br>',
					details: 'CEp não informado.'
				}, function(el, ev) {});
			}
		}
	);       


	
	
}


function setSelectedZoomItem(selectedItem) {
	var idCampo = selectedItem.inputId;

	switch (idCampo) {

		case "RESPONSAVEL_VIEW":
			$("#RESPONSAVEL").val(selectedItem["colleagueId"]);
			break;
	}

}