function inputFields(form){

    if(form.getFormMode() == 'ADD'){
        if(form.getValue("CODAUTO") == "Sim"){
            form.setValue("CODFILIAL", sugereValue( form.getValue("CODCOLIGADA") ) );    
        }
    }     

}

function sugereValue(codColigada){

    var c1 = DatasetFactory.createConstraint("CODCOLIGADA", codColigada,codColigada, ConstraintType.MUST);
	
    var constraints = new Array(c1);
    var dataset = DatasetFactory.getDataset("XPTO_001_FILIAL",["CODFILIAL","CODCOLIGADA"],constraints,null);

    var maiorValor = 0;
    for(var i = 0; i < dataset.rowsCount; i++) {
        var codigo = dataset.getValue(i, "CODFILIAL");
        codigo = parseInt(codigo);
        if(maiorValor < codigo){
            maiorValor = codigo;
        }
    }

    var novoCodigo = maiorValor + 1;

	return novoCodigo;

}