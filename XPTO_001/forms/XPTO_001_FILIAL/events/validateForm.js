function validateForm(form){
	
	var mensagemErro = "";

	mensagemErro += form.getValue("CODCOLIGADA") != "" ? "" : "O campo \"Empresa\" é de preenchimento obrigatório. <br>"; 
	mensagemErro += form.getValue("CODFILIAL") != "" ? "" : "O campo \"Código filial\" é de preenchimento obrigatório. <br>";
	mensagemErro += form.getValue("NOME") != "" ? "" : "O campo \"Nome\" é de preenchimento obrigatório. <br>";
	mensagemErro += form.getValue("CEP") != "" ? "" : "O campo \"CEP\" é de preenchimento obrigatório. <br>";
	mensagemErro += form.getValue("LOGRADOURO") != "" ? "" : "O campo \"Logradouro\" é de preenchimento obrigatório. <br>";
	mensagemErro += form.getValue("NUMERO") != "" ? "" : "O campo \"Número\" é de preenchimento obrigatório. <br>";
	mensagemErro += form.getValue("COMPLEMENTO") != "" ? "" : "O campo \"Complemento\" é de preenchimento obrigatório. <br>";
	mensagemErro += form.getValue("BAIRRO") != "" ? "" : "O campo \"Bairro\" é de preenchimento obrigatório. <br>";
	mensagemErro += form.getValue("UF") != "" ? "" : "O campo \"UF\" é de preenchimento obrigatório. <br>";
	mensagemErro += form.getValue("CIDADE") != "" ? "" : "O campo \"Cidade\" é de preenchimento obrigatório. <br>";
	mensagemErro += form.getValue("RESPONSAVEL_VIEW") != null ? "" : "O campo \"Responsável\" é de preenchimento obrigatório. <br>";
	mensagemErro += form.getValue("RESPONSAVEL") != "" ? "" : "O campo \"Responsável\" é de preenchimento obrigatório. <br>";

	var codFilial = form.getValue("CODFILIAL");
	var codColigada = form.getValue("CODCOLIGADA");
	if(codFilial != ""){
		if( isNaN(parseInt(codFilial)) ){
			mensagemErro += "Somente números inteiros são permitidos como \"Código da Filial\". <br>";
		}
		else{
			mensagemErro += !codigoDuplicado(codFilial,codColigada,form.getDocumentId()) ? "" : "O \"Código da Filial\" não pode ser duplicado. <br>";
		}
	}
	
	if(mensagemErro != ""){
		
		//Adiciona espaço superior e inferior
		mensagemErro = "<br>" + mensagemErro + "<br>";
		
		throw  mensagemErro;
	}
	
}

function codigoDuplicado(novoCodigo,codColigada,documentId){

	var c1 = DatasetFactory.createConstraint("CODFILIAL", novoCodigo,novoCodigo, ConstraintType.MUST);
	var c2 = DatasetFactory.createConstraint("documentid", documentId,documentId, ConstraintType.MUST_NOT);
	var c3 = DatasetFactory.createConstraint("CODCOLIGADA", codColigada,codColigada, ConstraintType.MUST);

	var constraints = new Array(c1,c2,c3);
	var dataset = DatasetFactory.getDataset("XPTO_001_FILIAL",["CODFILIAL"],constraints,null);

	return dataset.rowsCount > 0;

}